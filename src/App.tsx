import { Route, Routes } from "react-router-dom";
import Layout from "./components/Layout";
import Home from "./components/Home";
import Products from "./components/Products";
import ProductDetails from "./components/ProductDetails";
import Login from "./components/Login";
import Logout from "./components/Logout";
import { useEffect, useState } from "react";
import CheckAuth from "./services/CheckAuth";
import Register from "./components/Register";

function App() {
  const [showLogin, setShowLogin] = useState(false);
  function onLoginChange() {
    setShowLogin(CheckAuth());
  }

  useEffect(() => {
    onLoginChange();
  }, []);
  return (
    <>
      <Routes>
        <Route path="/" element={<Layout showLogin={showLogin} />}>
          <Route index element={<Home />} />
          <Route path="/products" element={<Products />} />
          <Route path="/products/:id" element={<ProductDetails />} />
          <Route
            path="/login"
            element={<Login handleLogin={onLoginChange} />}
          />
          <Route path="/register" element={<Register />} />
          <Route
            path="logout"
            element={<Logout handleLogin={onLoginChange} />}
          />
        </Route>
      </Routes>
    </>
  );
}

export default App;
