export interface Product {
  id: number;
  title: string;
  description?: string;
  brand: string;
  thumbnail: string;
  price: number;
  images: string[];
}
