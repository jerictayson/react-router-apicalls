export default interface Account {
  id: number;
  username: string;
  password: string;
  email: string;
  token: string;
  firstName: string;
  lastName: string;
  gender: string;
  image: string;
}
