import Cookies from "js-cookie";
export default function CheckAuth() {
  return Cookies.get("token") != undefined && Cookies.get("id") != undefined;
}
