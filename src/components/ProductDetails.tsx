import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import { Product } from "../types/Product";
import "../components/ProductDetails.css";
import { CircleLoader } from "react-spinners";
import CheckAuth from "../services/CheckAuth";
import Swal from "sweetalert2";

export default function ProductDetails() {
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();
  function addToCart() {
    if (!CheckAuth()) {
      navigate("/login");
      return;
    }

    Swal.fire("Message", "Product Added to cart", "success");
  }
  const defaultValue: Product = {
    id: 0,
    title: "",
    brand: "",
    thumbnail: "",
    price: 0,
    images: [""],
  };

  const [product, setProduct] = useState<Product>(defaultValue);
  const { id } = useParams<{ id: string }>();

  useEffect(() => {
    async function fetchDetails() {
      try {
        let result = await axios.get<Product>(
          `https://dummyjson.com/products/${id}`
        );

        setProduct(result.data);
        setLoading(false);
      } catch (e) {}
    }

    fetchDetails();
  }, [id]);

  return (
    <>
      {loading ? (
        <div className="d-flex justify-content-center min-vh-50">
          <CircleLoader color={"#007bff"} loading={loading} size="150px" />
        </div>
      ) : (
        <div className="container mt-5 border border-primary rounded-2 p-2">
          <div className="row">
            <div className="col-md-6">
              <div
                id="productImageCarousel"
                className="carousel slide"
                data-bs-ride="carousel"
              >
                <div className="carousel-inner">
                  {product.images.map((image, index) => (
                    <div
                      key={index}
                      className={`carousel-item ${index === 0 ? "active" : ""}`}
                    >
                      <img
                        src={image}
                        className=""
                        style={{
                          height: "50vh",
                          objectFit: "contain",
                          width: "100%",
                        }}
                        alt={`Product Image ${index}`}
                      />
                    </div>
                  ))}
                </div>
                <button
                  className="carousel-control-prev"
                  type="button"
                  data-bs-target="#productImageCarousel"
                  data-bs-slide="prev"
                >
                  <span
                    className="carousel-control-prev-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="visually-hidden">Previous</span>
                </button>
                <button
                  className="carousel-control-next"
                  type="button"
                  data-bs-target="#productImageCarousel"
                  data-bs-slide="next"
                >
                  <span
                    className="carousel-control-next-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="visually-hidden">Next</span>
                </button>
              </div>
            </div>
            <div className="col-md-6">
              <h2>{product.brand}</h2>
              <p>{product.description}</p>
              <p className="product-price">${product.price.toFixed(2)}</p>
              <button
                className="btn btn-primary"
                onClick={() => {
                  addToCart();
                }}
              >
                Add to Cart
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
