import { useEffect } from "react";
import CheckAuth from "../services/CheckAuth";
import Cookies from "js-cookie";
import { useNavigate } from "react-router-dom";
interface LogoutProps {
  handleLogin: () => void;
}
export default function Logout({ handleLogin }: LogoutProps) {
  const navigate = useNavigate();
  useEffect(() => {
    if (CheckAuth()) {
      Cookies.remove("token");
      Cookies.remove("id");
      handleLogin();
    }
    navigate("/");
  }, []);
  return <></>;
}
