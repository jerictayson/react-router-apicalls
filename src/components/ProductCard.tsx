import { useNavigate } from "react-router-dom";
import { Product } from "../types/Product";

export default function ProductCard({ product }: { product: Product }) {
  const navigate = useNavigate();
  return (
    <>
      <div className="card" style={{ width: "25rem" }}>
        <img
          src={product.thumbnail}
          className="card-img-top"
          style={{
            width: "100%",
            height: "25vh",
            objectFit: "cover",
          }}
        />
        <div className="card-body">
          <h4 className="card-title">{product.title}</h4>
          <h5 className="card-subtitle text-muted">{product.price}</h5>
          <p className="card-text">{product.description}</p>
          <button
            onClick={() => {
              navigate(`/products/${product.id}`);
            }}
            className="btn btn-success"
          >
            View Details
          </button>
        </div>
      </div>
    </>
  );
}
