import { useEffect, useState } from "react";
import Account from "../types/Account";
import axios from "axios";
import Cookies from "js-cookie";
import CheckAuth from "../services/CheckAuth";
import { useNavigate } from "react-router-dom";

interface LoginProps {
  handleLogin: () => void;
}

export default function Login({ handleLogin }: LoginProps) {
  const defaultValue: Account = {
    id: 0,
    username: "",
    password: "",
    email: "",
    token: "",
    firstName: "",
    lastName: "",
    gender: "",
    image: "",
  };

  const [user, setUser] = useState<Account>(defaultValue);
  const [error, setError] = useState("");
  const [spinner, setSpinner] = useState(""); 
  const navigate = useNavigate();

  function handleChange(key: keyof Account, value: string) {
    setError("");
    setUser((user) => ({ ...user, [key]: value }));
  }

  async function handleSubmit() {

    setSpinner("spinner-border spinner-border-sm");
    try {
      let res = await axios.post<Account>(
        "https://dummyjson.com/auth/login",
        {
          username: user.username,
          password: user.password,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      let token = res.data;
      Cookies.set("token", token.token);
      Cookies.set("id", res.data.id.toString());
      handleLogin();
      navigate("/");
    } catch (error) {
      setError("Invalid Username/Password");
    }

    setSpinner("");
  }

  useEffect(() => {
    if (CheckAuth()) {
      navigate("/");
    }
  }, []);

  return (
    <>
      <div className="row justify-content-center">
        <form
          className="form col-md-4"
          onSubmit={(e) => {
            e.preventDefault();
            handleSubmit();
          }}
        >
          <div className="mb-2 text-center">
            <span className="text-danger">{error}</span>
          </div>
          <div className="mb-3">
            <label htmlFor="username" className="form-label">
              Username:
            </label>
            <input
              type="text"
              name="username"
              id="username"
              className="form-control"
              onChange={(e) => {
                handleChange("username", e.target.value);
              }}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="password" className="form-label">
              Password:
            </label>
            <input
              type="password"
              name="password"
              id="password"
              className="form-control"
              onChange={(e) => {
                handleChange("password", e.target.value);
              }}
            />
          </div>
          <div className="mb-3">
            <button
              type="submit"
              className="d-block mx-auto btn btn-lg btn-success"
            >
              <span className={spinner}></span> Submit
            </button>
          </div>
        </form>
      </div>
    </>
  );
}
