import { Link, Outlet } from "react-router-dom";
export default function Layout({ showLogin }: { showLogin: boolean }) {
  return (
    <>
      <div className="container-fluid">
        <nav className="navbar navbar-expand-lg bg-body-tertiary">
          <div className="container-fluid">
            <a className="navbar-brand" href="#">
              Logo
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNavAltMarkup"
              aria-controls="navbarNavAltMarkup"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav">
                <Link className="nav-link active" aria-current="page" to="/">
                  Home
                </Link>
                <Link className="nav-link" to="/products">
                  Products
                </Link>
                <a className="nav-link" href="#">
                  Carts
                </a>
                <a className="nav-link disabled" aria-disabled="true">
                  Disabled
                </a>
              </div>
            </div>

            {!showLogin ? (
              <div className="navbar-nav">
                <Link className="nav-link" aria-current="page" to="/login">
                  Login
                </Link>
                <Link className="nav-link" to="/register">
                  Register
                </Link>
              </div>
            ) : (
              <div className="navbar-nav">
                <Link className="nav-link" aria-current="page" to="/logout">
                  Logout
                </Link>
              </div>
            )}
          </div>
        </nav>
        <Outlet />
      </div>
    </>
  );
}
