import { useState, useEffect } from "react";
import Account from "../types/Account";
import axios from "axios";
import CheckAuth from "../services/CheckAuth";
import { useNavigate } from "react-router-dom";
export default function Register() {
  const defaultValue: Account = {
    id: 0,
    username: "",
    password: "",
    email: "",
    token: "",
    firstName: "",
    lastName: "",
    gender: "",
    image: "",
  };

  const [account, setAccount] = useState<Account>(defaultValue);
  const navigate = useNavigate();
  useEffect(() => {
    if (CheckAuth()) {
      navigate("/");
    }
  }, []);

  function handleChange(key: keyof Account, value: string | number) {
    setAccount((acc) => ({ ...acc, [key]: value }));
  }

  async function handleSubmit() {
    try {
      if (isValid()) {
        let res = await axios.post<Account>(
          "https://dummyjson.com/users/add",
          account,
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );

        console.log(res.data);
      } else {
        console.log("Invalid");
      }
    } catch (error) {}
  }

  function isValid(): boolean {
    return (
      account.firstName != "" &&
      account.lastName != "" &&
      account.email != "" &&
      account.gender != "" &&
      account.email != "" &&
      account.password != ""
    );
  }
  return (
    <>
      <form
        className="form container"
        onSubmit={(e) => {
          e.preventDefault();
          handleSubmit();
        }}
      >
        <h1 className="text-center">Register here</h1>
        <div className="mb-3">
          <label htmlFor="firstname" className="form-label">
            Firstname:
          </label>
          <input
            type="text"
            name="firstname"
            id="firstname"
            className="form-control"
            onChange={(e) => {
              handleChange("firstName", e.target.value);
            }}
          />
        </div>

        <div className="mb-3">
          <label htmlFor="lastname " className="form-label">
            Lastname:
          </label>
          <input
            type="text"
            name="lastname"
            id="lastname"
            className="form-control"
            onChange={(e) => {
              handleChange("lastName", e.target.value);
            }}
          />
        </div>

        <div className="mb-3">
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="gender"
              id="male"
              value="Male"
              onChange={(e) => {
                handleChange("gender", e.target.value);
              }}
            />
            <label className="form-check-label" htmlFor="male">
              Male
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="gender"
              id="female"
              value="Female"
              onChange={(e) => {
                handleChange("gender", e.target.value);
              }}
            />
            <label className="form-check-label" htmlFor="female ">
              Female
            </label>
          </div>
        </div>
        <div className="mb-3">
          <label htmlFor="email" className="form-label">
            Email:
          </label>
          <input
            type="email"
            name="email"
            id="email"
            className="form-control"
            onChange={(e) => {
              handleChange("email", e.target.value);
            }}
          />
        </div>

        <div className="mb-3">
          <label htmlFor="username" className="form-label">
            Username:
          </label>
          <input
            type="text"
            name="username"
            id="username"
            className="form-control"
            onChange={(e) => {
              handleChange("username", e.target.value);
            }}
          />
        </div>

        <div className="mb-3">
          <label htmlFor="password" className="form-label">
            Password:
          </label>
          <input
            type="password"
            name="password"
            id="password"
            className="form-control"
            onChange={(e) => {
              handleChange("password", e.target.value);
            }}
          />
        </div>
        <div className="mb-3">
          <input
            type="submit"
            className="d-block mx-auto btn btn-lg btn-success"
            value="Submit"
          />
        </div>
      </form>
    </>
  );
}
