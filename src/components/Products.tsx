import axios from "axios";
import { useEffect, useState } from "react";
import { Product } from "../types/Product";
import ProductCard from "./ProductCard";

interface ProductAPI {
  products: Product[];
}
export default function Products() {
  const [search, setSearch] = useState("");
  const [products, setProducts] = useState<Product[]>([]);
  async function fetchData() {
    let result = await axios.get<ProductAPI>("https://dummyjson.com/products");
    setProducts(result.data.products.sort());
  }

  useEffect(() => {
    fetchData();
  }, []);

  function onSearchChange(e: string) {
    setSearch(e);
    searchData();
  }

  async function searchData() {
    try {
      if (search == "") {
        fetchData();
        return;
      }
      let result = await axios.get<ProductAPI>(
        `https://dummyjson.com/products/search?q=${search}`
      );

      setProducts(result.data.products.sort());
    } catch (error) {}
  }

  return (
    <>
      <div className="container mx-auto">
        <div className="row justify-content-end">
          <div className="col-md-3 mb-3 mt-3 d-flex">
            <input
              className="form-control me-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
              onChange={(e) => {
                onSearchChange(e.target.value);
              }}
            />
          </div>
        </div>
        <div className="row">
          {products.map((product) => (
            <div
              key={product.id}
              className="col-md-3 d-flex align-items-stretch justify-content-center mb-3"
            >
              <ProductCard product={product} />
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
